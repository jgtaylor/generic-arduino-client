#define ARDUINOJSON_ENABLE_PROGMEM 1
#include <Arduino.h>
#include <WiFi.h>
#include <WebSocketsClient.h>
#include <ArduinoJson.h>
#include <vector>
#include <string>

#define USE_SERIAL Serial
#define WEBSOCKET_SERVER ("192.168.2.122")
#define WEBSOCKET_PORT (1880)
#define WEBSOCKET_PATH ("/devices/water")
#define MK_LITERAL(...) #__VA_ARGS__

String config = MK_LITERAL([ "config", [ {"device" : "power", "type" : "button", "validCmds" : [ "on", "off", "toggle", "getState" ], "meta" : {"usage" : "MainsRelay"}}, {"device" : "time", "type" : "button", "validCmds" : [ "on", "off", "toggle", "getState" ], "meta" : {"usage" : "MainsRelay"}}, {"device" : "plus", "type" : "button", "validCmds" : [ "on", "off", "toggle", "getState" ], "meta" : {"usage" : "MainsRelay"}}, {"device" : "minus", "type" : "button", "validCmds" : [ "on", "off", "toggle", "getState" ], "meta" : {"usage" : "MainsRelay"}}, {"device" : "mode", "type" : "button", "validCmds" : [ "on", "off", "toggle", "getState" ], "meta" : {"usage" : "MainsRelay"}} ] ]);

std::string ssid = "X11";
std::string password = "secret99";
std::vector<int> pins = {22, 19, 5, 4, 13};
WebSocketsClient webSocket;

void hexdump(const void *mem, uint32_t len, uint8_t cols = 16)
{
    const uint8_t *src = (const uint8_t *)mem;
    USE_SERIAL.printf("\n[HEXDUMP] Address: 0x%08X len: 0x%X (%d)", (ptrdiff_t)src, len, len);
    for (uint32_t i = 0; i < len; i++)
    {
        if (i % cols == 0)
        {
            USE_SERIAL.printf("\n[0x%08X] 0x%08X: ", (ptrdiff_t)src, i);
        }
        USE_SERIAL.printf("%02X ", *src);
        src++;
    }
    USE_SERIAL.printf("\n");
}

void toggle(int pin)
{
    digitalWrite(pin, HIGH);
    // need to send over the device... maybe add the device, if I knew C++ better.
    //webSocket.sendTXT("[\"state\",{ \"device\":\"" + *device + "\",\"value\":1}]");
    delay(20);
    digitalWrite(pin, LOW);
    //webSocket.sendTXT("[\"state\",{ \"device\":\"" + *device + "\",\"value\":0}]");
}

int devID(const std::string &inString)
{
    // return pin numbers for each button / device.
    if (inString == "power")
        return 22;
    if (inString == "time")
        return 19;
    if (inString == "plus")
        return 5;
    if (inString == "minus")
        return 4;
    if (inString == "mode")
        return 13;
    return -1;
}

void webSocketEvent(WStype_t type, uint8_t *payload, size_t length)
{
    const int buffer = JSON_ARRAY_SIZE(2) + JSON_OBJECT_SIZE(2);
    DynamicJsonBuffer jsonBuffer(buffer);
    switch (type)
    {
    case WStype_DISCONNECTED:
    {
        USE_SERIAL.printf("[WSc] Disconnected!\n");
    }
    break;
    case WStype_CONNECTED:
    {
        USE_SERIAL.printf("[WSc] Connected to url: %s\n", payload);
        // send message to server when Connected
        //const uint8_t *y = reinterpret_cast<const uint8_t *>(config.c_str());
        webSocket.sendTXT(config);
    }
    break;
    case WStype_TEXT:
    {
        USE_SERIAL.printf("[WSc] get text: %s\n", payload);
        JsonArray &msg = jsonBuffer.parseArray(payload);

        const char *packetType = msg[0].as<char *>();
        const char *device = msg[1]["device"].as<char *>();

        if (std::string(packetType).compare("cmd") == 0)
        {
            // do the command stuff
            if (msg[1]["cmd"] == "toggle")
            {
                toggle(devID(std::string(device)));
                return;
            }
            if (msg[1]["cmd"] == "on")
            {
                digitalWrite(devID(std::string(device)), HIGH);
                std::string t = "[\"state\",{\"" + std::string(device);
                t.append("\":\", \"value\": 1} ]");
                const uint8_t *x = reinterpret_cast<const uint8_t *>(t.c_str());
                webSocket.sendTXT(x);
                return;
            }
            if (msg[1]["cmd"] == "off")
            {
                digitalWrite(devID(std::string(device)), LOW);
                std::string t = "[\"state\",{\"" + std::string(device);
                t.append("\":\", \"value\": 0} ]");
                const uint8_t *x = reinterpret_cast<const uint8_t *>(t.c_str());
                webSocket.sendTXT(x);
                return;
            }
        }
        //const char *command = msg[1]["cmd"];
        // send message to server
        // webSocket.sendTXT("message here");
    }
    break;
    case WStype_BIN:
    {
        USE_SERIAL.printf("[WSc] get binary length: %u\n", length);
        hexdump(payload, length);
    }

    // send data to server
    // webSocket.sendBIN(payload, length);
    break;
    case WStype_ERROR:
    {
        USE_SERIAL.println("Error");
    }
    break;
    case WStype_FRAGMENT_TEXT_START:
    case WStype_FRAGMENT_BIN_START:
    case WStype_FRAGMENT:
    case WStype_FRAGMENT_FIN:
        break;
    }
}

void setup()
{
    // put your setup code here, to run once:
    for (int n : pins)
    {
        pinMode(n, OUTPUT);
    }

    Serial.begin(115200);
    delay(10);

    // We start by connecting to a WiFi network
    Serial.print("Connecting to ");
    Serial.println(ssid.c_str());

    WiFi.begin(ssid.c_str(), password.c_str());

    while (WiFi.status() != WL_CONNECTED)
    {
        delay(500);
        Serial.print(".");
    }

    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
    // server address, port and URL
    webSocket.begin(WEBSOCKET_SERVER, WEBSOCKET_PORT, WEBSOCKET_PATH);

    // event handler
    webSocket.onEvent(webSocketEvent);
    // use HTTP Basic Authorization this is optional remove if not needed
    //webSocket.setAuthorization("user", "Password");

    // try ever 5000 again if connection has failed
    webSocket.setReconnectInterval(5000);
}

void loop()
{
    webSocket.loop();
}